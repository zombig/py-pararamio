import os
import string

from setuptools import find_packages, setup


def get_version(version_list):
    return '.'.join(map(str, version_list))


init = os.path.join(os.path.dirname(__file__), 'pararamio', 'constants.py')
version_line = list(filter(lambda l: l.startswith('VERSION'), open(init)))[0]
VERSION = get_version(''.join(list(filter(lambda c: c in string.digits + '.', version_line.split('=')[-1]))).split('.'))

setup(
    name='pararamio',
    version=VERSION,
    description='Pararam Library',
    url='https://gitlab.com/pararam-public/py-pararamio',
    author='Ilya Volnistov',
    author_email='i.volnistov@gaijin.team',
    license='MIT',
    packages=find_packages(),
    python_requires='>=3.6',
    zip_safe=False
)
